FROM dart:2.16.1 as builder

WORKDIR /app

RUN apt-get update
RUN apt-get install -y make

COPY . .
RUN dart --disable-analytics
RUN make -B
RUN make -B install

###

FROM debian

RUN apt-get update
RUN apt-get install -y libsqlite3-dev ca-certificates

RUN mkdir /app
WORKDIR /app

RUN mkdir /app/config
RUN mkdir /app/data
RUN mkdir /app/output

COPY --from=builder /app/build/mowoo /usr/local/bin
COPY --from=builder /usr/local/share/mowoo/ /app/data

ENTRYPOINT mowoo -v -c /app/config/config.toml