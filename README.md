# mowoo

A Matrix-Owncast-Whatever OBS Overlay

Use at your own risk! This is not ready yet for general usage. Be prepared to face bugs.

## Features

- Combine multiple stream chats into one feed!
- Can be used as chat overlay in OBS
- Supports Twitch, Owncast, Matrix, IRC chats
- Get Twitch emotes across all chats
- Deeply configurable
- Unlimited sources, sinks and middlewares

## Getting started

### Installation

Clone the repo:

```bash
git clone https://codeberg.org/yarmo/mowoo
cd mowoo/
```

Build and install mowoo:

```bash
make
make install
```

Note: you may need to run `sudo make install` if the current user doesn't have admin privileges.

Note: before mowoo can run, it needs to have a valid configuration file. See Usage > Sample Configuration File below to get started with one.

### Running mowoo without compiling

Run mowoo without compiling:

```bash
dart run ./bin/mowoo.dart
```

## Usage

### Sample configuration file

Location: `~/.config/mowoo/config.toml`

```toml
[general]
refresh_rate_seconds = 2

[[source]]
name = "My owncast"
type = "owncast"
url = "https://live.domain.org"
access_token = "myOwncastAccessToken"
middlewares = ["twitchEmotes"]

[[source]]
name = "My twitch"
type = "twitch"
channels = ["myTwitchUsername"]
username = "myTwitchUsername"
oauth_token = "myTwitchOauthToken"
middlewares = ["twitchEmotes"]

[[sink]]
type = "html"
path = "/home/user/.local/share/mowoo/sinks/index.html"
template_file = "data/templates/simple-ajax.html"
message_duration_seconds = 60
max_messages = 20

[[sink]]
type = "json"
path = "/home/user/.local/share/mowoo/sinks/chat.json"
indent_spaces = 2
message_duration_seconds = 60
max_messages = 20

[[middleware]]
name = "twitchEmotes"
type = "emotes"
twitch_channels = ["myTwitchUsername"]
twitch_oauth_token = "myTwitchOauthToken"
```

## Additional information

### Why doesn't mowoo support Twitch feature X?

Honestly, I just don't care enough about Twitch. And I don't use it.

I'd like to see feature X implemented so I invite any developer to make it happen if they feel strong about it being implemented.

## Roadmap

The "ASAP" list:

- [x] Export to HTML
- [x] Separate sources, sinks and message manager
- [x] Add Owncast source
- [x] Add IRC source
- [x] Add Twitch source
- [x] Only show messages for duration (config)
- [x] Add emotes
- [x] Support tiered emotes
- [ ] Add AUR package
- [ ] Add Dockerfile
- [x] Improve installation process
- [x] Improve templates
- [ ] Add avatars
- [ ] Add profanity filter (middleware)
- [ ] Add message persistency (sqlite)
- [ ] Documentation

The "if interest shown" list:

- [ ] Add Twitch badges, highlighted messages, bits...
- [ ] Add Facebook Live source
- [ ] Add YouTube source