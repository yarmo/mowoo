PREFIX ?= /usr/local
BINDIR ?= ${PREFIX}/bin
DATADIR ?= ${PREFIX}/share

BUILD_DIR := ./build
TARGET_EXEC := mowoo

$(BUILD_DIR)/$(TARGET_EXEC):
	@mkdir -p ./build
	@dart pub get
	@dart compile exe ./bin/mowoo.dart -o $(BUILD_DIR)/$(TARGET_EXEC)

.PHONY: install
install: $(BUILD_DIR)/$(TARGET_EXEC)
	@install -vD -m755 $(BUILD_DIR)/$(TARGET_EXEC) ${DESTDIR}${BINDIR}/mowoo
	@mkdir -p ${DESTDIR}${DATADIR}/mowoo/
	@cp -r ./data/media ${DESTDIR}${DATADIR}/mowoo/
	@cp -r ./data/templates ${DESTDIR}${DATADIR}/mowoo/
	@mkdir -p ${DESTDIR}${DATADIR}/mowoo/database/

.PHONY: clean
clean:
	@rm -r $(BUILD_DIR)