import 'dart:io';
import 'dart:async';
import 'package:args/args.dart';
import 'package:mowoo/mowoo.dart';
import 'package:logging/logging.dart';

Future<void> main(List<String> args) async {
  // Set up logger
  Logger.root.level = Level.WARNING;
  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
  });

  // Parse arguments
  var parser = ArgParser();
  parser.addOption("config", abbr: "c");
  parser.addFlag("verbose", abbr: "v");
  var argResults = parser.parse(args);

  if (argResults["verbose"] == true) {
    Logger.root.level = Level.ALL;
  }

  // Get config
  final config = getConfig(argResults["config"]);

  // Instantiate the orchestrator
  var orchestrator = Orchestrator(config.toMap());

  // Initialize the orchestrator
  await orchestrator.init();

  // Put orchestrator iterations on a timer
  final refreshRate =
      Duration(seconds: config.toMap()["general"]["refresh_rate_seconds"]);
  Timer.periodic(refreshRate, (timer) async {
    await orchestrator.iteration();
  });
}
