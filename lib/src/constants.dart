/// Current wowncast version
const mowooVersion = "0.1.0";

/// The default config location
const defaultConfigPath = "~/.config/mowoo/";

/// The default config file location
const defaultConfigFilename = "config.toml";

/// The default cache location
const defaultCachePath = "~/.cache/mowoo/";

/// The default data location
const defaultDataPath = "/usr/local/share/mowoo/";

/// The time in seconds between chat refreshes
const timeoutDataFetchSeconds = 60;

/// The default config file content
const defaultConfigFileContent = """[general]
refresh_rate_seconds = 2

# [[source]]
# name = "Libera"
# type = "irc"
# host = "irc.libera.chat"
# port = 6697
# ssl = true
# channels = [""]
# username = ""
# nickname = ""
# password = ""

# [[sink]]
# type = "html"
# path = "./index.html"
# template = "simple-ajax.html"
# max_messages = 20
""";
