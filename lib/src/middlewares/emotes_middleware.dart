import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mowoo/src/message.dart';
import 'package:mowoo/src/member.dart';
import 'package:mowoo/src/middleware.dart';

enum EmoteSource { twitch, bttv, ffz }

class EmotesMiddleware implements Middleware {
  @override
  final String name = "Emotes";
  @override
  final String type = "emotes";
  late Map<String, dynamic> config;

  // Middleware specific variables
  late Map<String, String> emotes;
  static const List<String> bttvHardcodedEmotes = [
    "5aa1d0e311237146531078b0", // Dance
    "5a311dd16405a95e4b0d4967", // WeSmart
    "5b1740221c5a6065a7bad4b5", // pepeD
    "5b490e73cf46791f8491f6f4", // HACKERMANS
    "5ba6d5ba6ee0c23989d52b10", // bongoTap
    "5b77ac3af7bddc567b1d5fb2", // pepeJAM
    "5e2914861df9195f1a4cd411", // ThisIsFine
    "5e5300e6751afe7d553e4351", // OOOO
    "5f1b0186cf6d2144653d2970", // catJAM
    "5f455410b2efd65d77e8cb14", // catKISS
    "55b6f480e66682f576dd94f5", // Clap
    "58b20d74d07b273e0dcfd57c", // ddHuh
  ];
  static const List<String> ffzHardcodedEmotes = [
    "28071", // GG!
    "57611", // Hahaa
    "128054", // OMEGALUL
    "139407", // LULW
    "162146", // AYAYA
    "210748", // Pog
    "298485", // pikachuS
    "381875", // KEKW
  ];

  // Constructor
  EmotesMiddleware(this.config) {
    // Set variables
    emotes = {};
  }

  @override
  Future<void> init() async {
    // Initialize variables
    emotes = {};

    // ===TWITCH===
    // Get Twitch client ID
    var twitchClientId = await _getTwitchClientId(config["twitch_oauth_token"]);

    // Get broadcaster IDs
    Map<String, String?> broadcasterId = {};
    if (twitchClientId != null) {
      for (var channelName in config["twitch_channels"]) {
        broadcasterId[channelName] = await _getTwitchBroadCasterId(
            channelName, config["twitch_oauth_token"], twitchClientId);
      }
    }

    if (twitchClientId != null) {
      // Get global emotes
      emotes.addAll(
          await _getTwitchEmotes(config["twitch_oauth_token"], twitchClientId));

      // Get channel emotes
      for (var channelName in config["twitch_channels"]) {
        emotes.addAll(await _getTwitchEmotes(
            config["twitch_oauth_token"], twitchClientId,
            broadcasterId: broadcasterId[channelName]));
      }
    }

    // ===BTTV===
    // Get global emotes
    emotes.addAll(await _getBttvEmotes());

    // Get channel emotes
    for (var channelName in List<String>.from(config["twitch_channels"])) {
      emotes.addAll(
          await _getBttvEmotes(broadcasterId: broadcasterId[channelName]));
    }

    // Get hardcoded emotes
    for (var emoteId in bttvHardcodedEmotes) {
      emotes.addAll(await _getBttvEmotes(emoteId: emoteId));
    }

    // Get user config emotes
    if (config["bttv_emotes"] != null) {
      for (var emoteId in List<String>.from(config["bttv_emotes"])) {
        emotes.addAll(await _getBttvEmotes(emoteId: emoteId));
      }
    }

    // ===FFZ===
    // Get global emotes
    emotes.addAll(await _getFfzEmotes());

    // Get hardcoded emotes
    for (var emoteId in ffzHardcodedEmotes) {
      emotes.addAll(await _getFfzEmotes(emoteId: emoteId));
    }

    // Get user config emotes
    if (config["ffz_emotes"] != null) {
      for (var emoteId in List<String>.from(config["ffz_emotes"])) {
        emotes.addAll(await _getFfzEmotes(emoteId: emoteId));
      }
    }
  }

  @override
  Future<Map> process(Message? message, Member? member) async {
    if (message == null) {
      return {
        "message": message,
        "member": member,
      };
    }

    var words = message.text.split(" ");

    for (var i = 0; i < words.length; i++) {
      if (emotes.containsKey(words[i])) {
        words[i] = "<img src='${emotes[words[i]]}' alt='${words[i]}'>";
      }
    }

    message.text = words.join(" ");

    return {
      "message": message,
      "member": member,
    };
  }

  Future<String?> _getTwitchClientId(String accessToken) async {
    final urlApi = Uri.parse("https://id.twitch.tv/oauth2/validate");
    final apiResponse = await http.get(urlApi, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer $accessToken"
    });
    if (apiResponse.statusCode == 200) {
      final apiResponseData = json.decode(apiResponse.body);
      return apiResponseData["client_id"];
    } else {
      return null;
    }
  }

  Future<String?> _getTwitchBroadCasterId(
      String channelName, String accessToken, String clientId) async {
    final urlApi =
        Uri.parse("https://api.twitch.tv/helix/users?login=$channelName");
    final apiResponse = await http.get(urlApi, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer $accessToken",
      "Client-Id": clientId
    });
    if (apiResponse.statusCode == 200) {
      final apiResponseData = json.decode(apiResponse.body);
      return apiResponseData["data"][0]["id"];
    } else {
      return null;
    }
  }

  Future<Map<String, String>> _getTwitchEmotes(
      String accessToken, String clientId,
      {String? broadcasterId, String? emoseSetId}) async {
    // Init variables
    final Uri urlApi;

    // Set URL depending on request
    if (broadcasterId != null) {
      urlApi = Uri.parse(
          "https://api.twitch.tv/helix/chat/emotes?broadcaster_id=$broadcasterId");
    } else if (emoseSetId != null) {
      urlApi = Uri.parse(
          "https://api.twitch.tv/helix/chat/emotes/set?emote_set_id=$emoseSetId");
    } else {
      urlApi = Uri.parse("https://api.twitch.tv/helix/chat/emotes/global");
    }

    // Make HTTP request
    var apiResponse = await http.get(urlApi, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer $accessToken",
      "Client-Id": clientId
    });
    if (apiResponse.statusCode != 200) {
      return {};
    }
    // Init output variable
    Map<String, String> emotesData = {};

    // Interpret data
    var apiResponseData = json.decode(apiResponse.body);
    for (var emote in apiResponseData["data"]) {
      emotesData[emote["name"]] = emote["images"]["url_2x"];
    }
    return emotesData;
  }

  Future<Map<String, String>> _getBttvEmotes(
      {String? broadcasterId, String? emoteId}) async {
    // Init variables
    final Uri urlApi;

    // Set URL depending on request
    if (broadcasterId != null) {
      urlApi = Uri.parse(
          "https://api.betterttv.net/3/cached/users/twitch/$broadcasterId");
    } else if (emoteId != null) {
      urlApi = Uri.parse("https://api.betterttv.net/3/emotes/$emoteId");
    } else {
      urlApi = Uri.parse("https://api.betterttv.net/3/cached/emotes/global");
    }

    // Make HTTP request
    var apiResponse =
        await http.get(urlApi, headers: {"Content-Type": "application/json"});
    if (apiResponse.statusCode != 200) {
      return {};
    }

    // Init output variable
    Map<String, String> emotesData = {};

    // Interpret data
    var apiResponseData = json.decode(apiResponse.body);
    List<dynamic> apiData;
    if (broadcasterId != null) {
      apiData = apiResponseData["channelEmotes"];
    } else if (emoteId != null) {
      return {
        apiResponseData["code"]:
            "https://cdn.betterttv.net/emote/${apiResponseData["id"]}/2x"
      };
    } else {
      apiData = apiResponseData;
    }
    for (var emote in apiData) {
      emotesData[emote["code"]] =
          "https://cdn.betterttv.net/emote/${emote["id"]}/2x";
    }
    return emotesData;
  }

  Future<Map<String, String>> _getFfzEmotes(
      {String? emoteId, String? emoteSetId}) async {
    // Init variables
    final Uri urlApi;

    // Set URL depending on request
    if (emoteId != null) {
      urlApi = Uri.parse("https://api.frankerfacez.com/v1/emote/$emoteId");
    } else if (emoteSetId != null) {
      urlApi = Uri.parse("https://api.frankerfacez.com/v1/set/$emoteSetId");
    } else {
      urlApi = Uri.parse("https://api.frankerfacez.com/v1/set/global");
    }

    // Make HTTP request
    var apiResponse =
        await http.get(urlApi, headers: {"Content-Type": "application/json"});
    if (apiResponse.statusCode != 200) {
      return {};
    }

    // Init output variable
    Map<String, String> emotesData = {};

    // Interpret data
    var apiResponseData = json.decode(apiResponse.body);
    if (emoteId != null) {
      var emote = apiResponseData["emote"];
      if (emote["urls"]["2"] != null) {
        emotesData[emote["name"]] = "https:${emote["urls"]["2"]}";
      } else {
        emotesData[emote["name"]] = "https:${emote["urls"]["1"]}";
      }
    } else {
      for (var emoteSet in apiResponseData["default_sets"]) {
        for (var emote in apiResponseData["sets"]["$emoteSet"]["emoticons"]) {
          if (emote["urls"]["2"] != null) {
            emotesData[emote["name"]] = "https:${emote["urls"]["2"]}";
          } else {
            emotesData[emote["name"]] = "https:${emote["urls"]["1"]}";
          }
        }
      }
    }
    return emotesData;
  }
}
