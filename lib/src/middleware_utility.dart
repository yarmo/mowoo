import 'message.dart';
import 'member.dart';
import 'middleware.dart';

abstract class MiddlewareUtility {
  static Future<Map> apply(
      Message? message,
      Member? member,
      List<String> requestedMiddlewares,
      Map<String, Middleware> availableMiddlewares) async {
    // For each requested middleware
    for (var middlewareName in requestedMiddlewares) {
      // Get the middleware
      var middlewareFn = availableMiddlewares[middlewareName];
      if (middlewareFn != null) {
        // Process the message and member
        var middlewareResult = await middlewareFn.process(message, member);
        message = middlewareResult["message"];
        member = middlewareResult["member"];
      }
    }

    // Return the message and member
    return {
      "message": message,
      "member": member,
    };
  }
}
