abstract class Sink {
  /// Identifier of the sink
  final String type = "";

  // Constructor
  Sink();
}
