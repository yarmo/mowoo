import 'dart:io';
import 'package:mowoo/mowoo.dart';
import 'package:logging/logging.dart';

class Orchestrator {
  final log = Logger('Orchestrator');

  // The configuration map that defines the orchestrator's actions
  Map config;
  Map<String, Middleware> middlewares = {};
  Set<Source> sources = <Source>{};

  // Constructor
  Orchestrator(this.config) {
    // Set up database
    DbUtility.init(config);

    // Collect middlewares
    if (config["middleware"] != null) {
      for (var middlewareConfig in config["middleware"]) {
        switch (middlewareConfig["type"]) {
          case "helloworld":
            middlewares[middlewareConfig["name"]] =
                HelloWorldMiddleware(middlewareConfig);
            break;
          case "emotes":
            middlewares[middlewareConfig["name"]] =
                EmotesMiddleware(middlewareConfig);
            break;
          default:
        }
      }
    }

    // Collect sources
    if (config["source"] != null) {
      for (var sourceConfig in config["source"]) {
        switch (sourceConfig["type"]) {
          case "irc":
            sources.add(IrcSource(sourceConfig, middlewares: middlewares));
            break;
          case "matrix":
            sources.add(MatrixSource(sourceConfig, middlewares: middlewares));
            break;
          case "owncast":
            sources.add(OwncastSource(sourceConfig, middlewares: middlewares));
            break;
          case "twitch":
            sources.add(TwitchSource(sourceConfig, middlewares: middlewares));
            break;
          default:
        }
      }
    }
  }

  Future<void> init() async {
    log.info("Orchestrator is initializing middlewares");
    // Initialize middlewares
    for (var middlewareName in middlewares.keys) {
      await middlewares[middlewareName]!.init();
    }

    log.info("Orchestrator is initializing sources");
    // Initialize sources
    for (var source in sources) {
      await source.init();
      // await source.fetchHistoricalMessages();
      await source.updateMembers();
    }
  }

  Future<void> iteration() async {
    log.info("Orchestrator is running an iteration");
    // Fetch new messages for each source
    for (var source in sources) {
      try {
        await source.fetchNewMessages();
        await source.updateMembers();
      } catch (_) {}
    }

    // Collect messages and members
    for (var source in sources) {
      // Fetch the most recent messages and members
      DbUtility.storeMembers(source.members, config);
      DbUtility.storeMessages(source.messages, config);

      // Clear the messages and members
      source.messages.clear();
      source.members.clear();
    }

    // Get messages and members from the database
    var members = DbUtility.getMembers(config);
    var messages = DbUtility.getMessages(config);

    // Sort the messages
    messages.sort((a, b) => a.timestamp.compareTo(b.timestamp));

    // Render each sink
    if (config["sink"] != null) {
      for (var sinkConfig in config["sink"]) {
        var sinkMessages = messages;

        // Handle max_messages config
        if (sinkConfig["max_messages"] != null) {
          int maxMessages = sinkConfig["max_messages"];
          if (sinkMessages.length > maxMessages) {
            sinkMessages = sinkMessages.sublist(
                sinkMessages.length - maxMessages, sinkMessages.length);
          }
        }

        // Handle message_duration_seconds config
        if (sinkConfig["message_duration_seconds"] != null) {
          int messageDurationSeconds = sinkConfig["message_duration_seconds"];
          sinkMessages.retainWhere((m) => m.timestamp.isAfter(DateTime.now()
              .subtract(Duration(seconds: messageDurationSeconds))));
        }

        // Choose the correct sink renderer
        String? sinkRender;
        switch (sinkConfig["type"]) {
          case "text":
            sinkRender = TextSink.render(sinkMessages, members);
            break;
          case "json":
            sinkRender = JsonSink.render(sinkMessages, members,
                indentSpaces: sinkConfig["indent_spaces"]);
            break;
          case "html":
            sinkRender = await HtmlSink.render(sinkMessages, members,
                templateName: sinkConfig["template"],
                templateFilePath: sinkConfig["template_file"],
                config: config);
            break;
          default:
        }

        // Save the render to file
        if (sinkRender != null) {
          var outputFile = File(sinkConfig["path"]);
          outputFile.writeAsStringSync(sinkRender);
        }
      }
    }
  }
}
