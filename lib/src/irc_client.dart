import 'dart:async';
import 'dart:convert';
import 'dart:io';

class IrcClient {
  late Socket socket;
  String host;
  int port;
  bool ssl;
  String nickname;
  String? username;
  String? realname;
  String? password;
  List<String> channels;
  List<Map> storedMessages = [];
  List<String>? capabilityRequests = [];
  bool verbose;

  IrcClient({
    required this.host,
    required this.port,
    this.ssl = false,
    required this.nickname,
    this.username,
    this.realname,
    this.password,
    required this.channels,
    this.capabilityRequests,
    this.verbose = false,
  });

  Future<void> connect() async {
    // Initialize socket connection
    if (ssl == true) {
      socket = await SecureSocket.connect(host, port);
    } else {
      socket = await Socket.connect(host, port);
    }

    // Register event listener
    socket.listen(
      // Handle data events
      (data) {
        final serverResponse = utf8.decode(data).trim();
        for (var line in serverResponse.split('\n')) {
          _receive(line);
        }
      },

      // Handle error events
      onError: (error) {
        socket.destroy();
      },

      // Handle server ending connection
      onDone: () {
        socket.destroy();
      },
    );

    // Send password
    String? pw = password;
    if (pw != null) {
      await _send(['PASS $pw'], secrets: [pw]);
    }

    // Send username and nickname
    await _send([
      'USER $nickname ${username ?? nickname} $host :${realname ?? nickname}',
      'NICK $nickname'
    ]);

    // Send capability requests
    // Source: https://ircv3.net/specs/extensions/capability-negotiation
    if (capabilityRequests != null) {
      for (var capReq in capabilityRequests!) {
        await _send(["CAP REQ :$capReq"]);
      }
    }
  }

  // Send a line to the server
  Future<void> _send(List<String> lines, {List<String>? secrets}) async {
    for (var line in lines) {
      var lineLog = line;
      if (secrets != null) {
        for (var secret in secrets) {
          lineLog = lineLog.replaceAll(secret, "REDACTED");
        }
      }
      _printIfVerbose('< $lineLog');
      socket.writeln(line);
    }
    await socket.flush();
  }

  // Receive a line from the server
  Future<void> _receive(String line) async {
    // Perform "command" regex on received line
    RegExp reCmd = RegExp(r"^(PING|KILL) :(.*)");
    RegExpMatch? matchCmd = reCmd.firstMatch(line);
    if (matchCmd != null) {
      _printIfVerbose('> $line');
      switch (matchCmd[1]) {
        case "PING": // PING
          _send(["PONG ${matchCmd[2]}"]);
          break;
        default:
      }
      return;
    }

    // Perform "reply" regex on received line
    RegExp reRpl =
        RegExp(r"^(?:@([^\s]*) )?:([^\s]*) ([^\s]*) ([^\:]*)(?::(.*))?");
    RegExpMatch? matchRpl = reRpl.firstMatch(line);
    if (matchRpl == null) {
      _printIfVerbose('>??? $line');
      return;
    }

    // Get components of received line
    String? tags = matchRpl[1];
    String? sender = matchRpl[2];
    String? command = matchRpl[3];
    String? target = matchRpl[4];
    String? content = matchRpl[5];

    // Print
    _printIfVerbose('> <$sender>(${tags ?? ""})--[$command] $content');

    switch (command) {
      case "376": // RPL_ENDOFMOTD
        var delay = 0;
        for (var channel in channels) {
          Timer(Duration(seconds: delay), () {
            _send(["JOIN #$channel"]);
          });
          delay += 1;
        }
        break;
      case "PRIVMSG": // PRIVMSG
        storedMessages.add({
          "sender": sender!.split("!")[0],
          "message": content,
          "tags": _processTags(tags)
        });
        break;
      default:
    }
  }

  void _printIfVerbose(String message) {
    if (verbose == true) {
      print(message);
    }
  }

  /// Process the IRCv3 tags
  ///
  /// Reference: https://ircv3.net/specs/extensions/message-tags.html
  Map<String, dynamic> _processTags(String? tags) {
    // Initialize
    Map<String, dynamic> output = {};

    // Return early on empty tags
    if (tags == null) {
      return output;
    }

    // Generate a map of tags and return it
    for (var tag in tags.split(';')) {
      var tagComponents = tag.split("=");

      // Handle missing or empty values (example: "key=;")
      if (tagComponents.length == 1 || tagComponents[1] == "") {
        output[tagComponents[0]] = null;
      }

      output[tagComponents[0]] = tagComponents[1];
    }

    return output;
  }

  bool hasStoredMessages() {
    return storedMessages.isNotEmpty;
  }

  List<Map> getStoredMessages() {
    var output = storedMessages;
    storedMessages = [];
    return output;
  }
}
