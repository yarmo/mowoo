import 'package:color/color.dart';
import 'package:mowoo/src/member.dart';
import 'package:mowoo/src/message.dart';

class Middleware {
  /// Name of the source
  final String name = "";

  /// Type of the source
  final String type = "";

  // Constructor
  Middleware();

  /// Initialize the middleware
  ///
  /// Useful if the middleware has some preparation to do that requires async.
  Future<void> init() async {}

  /// Process the message
  Future<Map> process(Message? message, Member? member) async {
    // TODO Find better solution for this hack
    return {
      "message": Message("", "", "", DateTime.now(), "", "", Color.hex("")),
      "member": Member("", "", "", "", Color.hex(""))
    };
  }
}
