import 'package:color/color.dart';
import 'package:sqlite3/sqlite3.dart';
import 'package:mowoo/mowoo.dart';
import 'constants.dart' as constants;
import 'package:logging/logging.dart';

abstract class DbUtility {
  static final log = Logger('DbUtility');

  // Init database
  static init(Map? config) {
    log.info("Loading database file at ${getPath(config)}mowoo.sqlite");
    final db = sqlite3.open('${getPath(config)}mowoo.sqlite');

    log.info("Initializing the database if needed");
    try {
      db.execute('''
CREATE TABLE members (
  id TEXT NOT NULL PRIMARY KEY,
  display_name TEXT,
  color TEXT NOT NULL,
  source_type TEXT NOT NULL,
  source_id TEXT NOT NULL,
  source_color TEXT NOT NULL
);
    ''');
    } catch (_) {}
    try {
      db.execute('''
CREATE TABLE messages (
  id TEXT NOT NULL PRIMARY KEY,
  text TEXT NOT NULL,
  member_id TEXT NOT NULL,
  timestamp DATETIME NOT NULL,
  source_type TEXT NOT NULL,
  source_id TEXT NOT NULL,
  source_color TEXT NOT NULL,
  FOREIGN KEY (member_id) REFERENCES members(id)
);
    ''');
    } catch (_) {}

    db.dispose();
  }

  // Store members
  static storeMembers(Set<Member> members, Map? config) {
    final db = sqlite3.open('${getPath(config)}mowoo.sqlite');

    var stmt = db.prepare('''
INSERT INTO members 
(id, display_name, color, source_type, source_id, source_color)
VALUES (?, ?, ?, ?, ?, ?) 
ON CONFLICT(id) DO UPDATE SET display_name=?, color=?;
''', persistent: true);

    for (var member in members) {
      try {
        stmt.execute([
          member.id,
          member.displayName,
          member.color.toHexColor().toCssString(),
          member.sourceType,
          member.sourceId,
          member.sourceColor.toHexColor().toCssString(),
          member.displayName,
          member.color.toHexColor().toCssString(),
        ]);
      } catch (_) {}
    }

    stmt.dispose();
    db.dispose();
  }

  // Store messages
  static storeMessages(Set<Message> messages, Map? config) {
    final db = sqlite3.open('${getPath(config)}mowoo.sqlite');

    var stmt = db.prepare('''
INSERT INTO messages 
(id, text, member_id, timestamp, source_type, source_id, source_color)
VALUES (?, ?, ?, ?, ?, ?, ?)
''', persistent: true);

    for (var message in messages) {
      try {
        stmt.execute([
          message.id,
          message.text,
          message.memberId,
          message.timestamp.toIso8601String(),
          message.sourceType,
          message.sourceId,
          message.sourceColor.toHexColor().toCssString(),
        ]);
      } catch (_) {}
    }

    stmt.dispose();
    db.dispose();
  }

  // Get members
  static Set<Member> getMembers(Map? config) {
    final db = sqlite3.open('${getPath(config)}mowoo.sqlite');

    final members = <Member>{};
    final ResultSet resultSet = db.select('SELECT * FROM members');
    for (var el in resultSet) {
      members.add(Member(el["id"], el["display_name"] ?? "", el["source_type"],
          el["source_id"], Color.hex(el["source_color"]),
          memberColor: Color.hex(el["color"] ?? "#fff")));
    }

    db.dispose();

    return members;
  }

  // Get messages
  static List<Message> getMessages(Map? config) {
    final db = sqlite3.open('${getPath(config)}mowoo.sqlite');

    final messages = <Message>[];
    final ResultSet resultSet = db.select('SELECT * FROM messages');
    for (var el in resultSet) {
      messages.add(Message(
          el["id"],
          el["text"],
          el["member_id"],
          DateTime.parse(el["timestamp"]),
          el["source_type"],
          el["source_id"],
          Color.hex(el["source_color"])));
    }

    db.dispose();

    return messages;
  }

  // Get database directory
  static String getPath(Map? config) {
    final String dbPath;

    if (config != null && config["general"]["data_path"] != null) {
      dbPath = "${config["general"]["data_path"]}/database/";
    } else {
      dbPath = "${constants.defaultDataPath}database/";
    }

    return dbPath;
  }
}
