import 'dart:convert';
import 'package:color/color.dart';
import 'package:http/http.dart' as http;
import '../middleware.dart';
import '../middleware_utility.dart';
import '../source.dart';
import '../member.dart';
import '../message.dart';

enum _requestType { messages, members }

class OwncastSource implements Source {
  // Common variables
  @override
  final String name = "Owncast";
  @override
  final String type = "owncast";
  @override
  final Color color = Color.hex("#7871ff");
  @override
  late final String id;
  @override
  final messages = <Message>{};
  @override
  final members = <Member>{};
  @override
  late final List<String> requestedMiddlewares;
  @override
  late final Map<String, Middleware> availableMiddlewares;

  // Source specific variables
  late final String url;
  late final String accessToken;

  // Constructor
  OwncastSource(Map config, {Map<String, Middleware>? middlewares}) {
    // Set variables
    url = config["url"];
    accessToken = config["access_token"];
    id = url;
    requestedMiddlewares = List<String>.from(config["middlewares"] ?? []);
    availableMiddlewares = middlewares ?? {};
  }

  @override
  int get hashCode => Object.hash(id, type);

  @override
  bool operator ==(Object other) {
    return other is OwncastSource && id == other.id && type == other.type;
  }

  @override
  Future<void> init() async {}

  @override
  Future<void> fetchHistoricalMessages() async {
    await _requestOwncastApi(_requestType.messages);
  }

  @override
  Future<void> fetchNewMessages() async {
    await _requestOwncastApi(_requestType.messages);
  }

  @override
  Future<void> updateMembers() async {
    await _requestOwncastApi(_requestType.members);
  }

  Future<void> _requestOwncastApi(_requestType requestType) async {
    // Fetch messages using API
    final Uri urlApi;
    switch (requestType) {
      case _requestType.messages:
        urlApi = Uri.parse("$url/api/integrations/chat");
        break;
      case _requestType.members:
        urlApi = Uri.parse("$url/api/integrations/clients");
        break;
      default:
        return;
    }
    var apiResponse = await http.get(urlApi, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer $accessToken"
    });

    // For each message
    if (apiResponse.statusCode != 200) {
      return;
    }

    // Decode response body into UTF8 JSON
    final body = Utf8Decoder().convert(apiResponse.bodyBytes);
    var apiResponseData = json.decode(body);

    switch (requestType) {
      case _requestType.messages:
        // Process API response as messages
        for (var message in apiResponseData) {
          if (message["type"] == "CHAT") {
            String body = message["body"];

            // Fix relative URLs to emojis
            body = body.replaceAll('src="/img/emoji/', 'src="$url/img/emoji/');

            // Make new variables
            // TODO Add custom color to members
            var newMessage = Message(message["id"], body, message["user"]["id"],
                DateTime.parse(message["timestamp"]), type, id, color);
            var newMember = Member(message["user"]["id"],
                message["user"]["displayName"], type, id, color);

            // Apply middlewares
            var middlewareResult = await MiddlewareUtility.apply(newMessage,
                newMember, requestedMiddlewares, availableMiddlewares);
            newMessage = middlewareResult["message"];
            newMember = middlewareResult["member"];

            // Add message to messages
            messages.add(newMessage);

            // Add member to members
            members.add(newMember);
          }
        }
        break;
      case _requestType.members:
        // Process API response as members
        for (var member in apiResponseData) {
          // Add member to members
          members.add(Member(member["user"]["id"],
              member["user"]["displayName"], type, id, color));
        }
        break;
      default:
    }
  }

  @override
  Map<String, dynamic> transformAvatarId(Member member) {
    // var avatarMXC = m.content["avatar_url"];
    // var avatarMatches = avatarRe.firstMatch(avatarMXC);

    // var avatarRe = RegExp(r"mxc\:\/\/(.*)\/(.*)");

    // if (avatarMatches != null) {
    //   var serverName = avatarMatches.group(1)!;
    //   var mediaId = avatarMatches.group(2)!;

    //   var avatarFile = await client.getContent(serverName, mediaId);
    //   print(avatarFile.contentType);
    //   if (avatarFile.contentType == "image/jpeg") {
    //     Image image = decodeJpg(avatarFile.data)!;
    //     File('cool-awesome-avatar-yo.png').writeAsBytesSync(encodePng(image));
    //   }
    // }

    // TODO: implement getAvatarUrlForMember
    throw UnimplementedError();
  }
}
