import 'dart:convert' show HtmlEscape;
import 'package:color/color.dart';
import 'dart:math';
import '../middleware.dart';
import '../middleware_utility.dart';
import '../source.dart';
import '../member.dart';
import '../message.dart';
import '../irc_client.dart';

class TwitchSource implements Source {
  // Common variables
  @override
  final String name = "Twitch";
  @override
  final String type = "twitch";
  @override
  final Color color = Color.hex("#9146ff");
  @override
  late final String id;
  @override
  final messages = <Message>{};
  @override
  final members = <Member>{};
  @override
  late final List<String> requestedMiddlewares;
  @override
  late final Map<String, Middleware> availableMiddlewares;

  // Source specific variables
  late final String username;
  late final String accessToken;
  late final List<String> channels;
  late final IrcClient client;

  // Constructor
  TwitchSource(Map config, {Map<String, Middleware>? middlewares}) {
    // Set variables
    username = config["username"];
    accessToken = config["oauth_token"];
    if (config["channels"].runtimeType == String) {
      channels = [config["channels"]];
    } else {
      channels = List<String>.from(config["channels"]);
    }
    id = channels.join("");
    requestedMiddlewares = List<String>.from(config["middlewares"] ?? []);
    availableMiddlewares = middlewares ?? {};

    // Initialize the IRC client
    client = IrcClient(
      host: "irc.chat.twitch.tv",
      port: 6697,
      ssl: true,
      nickname: username,
      username: username,
      realname: username,
      password: "oauth:$accessToken",
      channels: channels,
      capabilityRequests: ["twitch.tv/membership", "twitch.tv/tags"],
      verbose: false,
    );
  }

  @override
  int get hashCode => Object.hash(id, type);

  @override
  bool operator ==(Object other) {
    return other is TwitchSource && id == other.id && type == other.type;
  }

  @override
  Future<void> init() async {
    // Connect the IRC client
    client.connect();
  }

  @override
  Future<void> fetchHistoricalMessages() async {}

  @override
  Future<void> fetchNewMessages() async {
    if (!client.hasStoredMessages()) {
      return;
    }
    var sanitizer = const HtmlEscape();

    for (var m in client.getStoredMessages()) {
      // Get the message and username
      String secret = Random.secure().nextInt(999999).toString();
      String message = m['message'] ?? '';

      // Process Twitch badges
      if (m['tags']['badges'] != null && m['tags']['badges'] != "") {
        // TODO Badges
      }

      // Process Twitch emotes
      if (m['tags']['emotes'] != null && m['tags']['emotes'] != "") {
        List<Map<String, dynamic>> emotesList = [];
        // Iterate over each emote
        for (var emoteComponents
            in (m['tags']['emotes'] as String).split('/').reversed) {
          final emote = emoteComponents.split(':');
          // Iterate over each instance of the emote
          for (var emoteRanges in emote[1].split(',').reversed) {
            final start = int.parse(emoteRanges.split('-')[0]);
            final end = int.parse(emoteRanges.split('-')[1]) + 1;
            final replacement =
                '%%emoji-$secret-start%%${emote[0]}%%emoji-$secret-end%%';
            emotesList
                .add({"start": start, "end": end, "replacement": replacement});
          }
        }

        // Sort the emotes to replace
        emotesList.sort((a, b) => a["start"].compareTo(b["start"]));

        // Iterate over the emotes to replace
        for (var emote in emotesList.reversed) {
          message = message.replaceRange(
              emote["start"], emote["end"], emote["replacement"]);
        }
      }
      message = sanitizer.convert(message);
      message = message.replaceAll("%%emoji-$secret-start%%",
          "<img src=\"https://static-cdn.jtvnw.net/emoticons/v2/");
      message =
          message.replaceAll("%%emoji-$secret-end%%", "/static/light/2.0\" />");
      // Prepare new variable
      var newMessage = Message(m['tags']['id'], message, m['tags']['user-id'],
          DateTime.now(), type, id, color);

      var username = m['tags']['display-name'] ?? m['sender'] ?? 'anonymous';
      Color? memberColor;
      if (m['tags']['color'] != null && m['tags']['color'] != "") {
        memberColor = Color.hex(m['tags']['color']);
      }
      var newMember = Member(m['tags']['user-id'], username, type, id, color,
          memberColor: memberColor);

      // Apply middlewares
      var middlewareResult = await MiddlewareUtility.apply(
          newMessage, newMember, requestedMiddlewares, availableMiddlewares);
      newMessage = middlewareResult["message"];
      newMember = middlewareResult["member"];

      // Add message to messages
      messages.add(newMessage);

      // Add member to members
      members.add(newMember);
    }
  }

  @override
  Future<void> updateMembers() async {}

  @override
  Map<String, dynamic> transformAvatarId(Member member) {
    // TODO: implement getAvatarUrlForMember
    throw UnimplementedError();
  }

  // String _processMessage(String message) {}
}
