import 'package:color/color.dart';
import 'package:mowoo/mowoo.dart';
import '../middleware.dart';
import '../middleware_utility.dart';
import '../source.dart';
import '../member.dart';
import '../message.dart';

class IrcSource implements Source {
  // Common variables
  @override
  final String name = "IRC";
  @override
  final String type = "irc";
  @override
  final Color color = Color.hex("#005960");
  @override
  late final String id;
  @override
  final messages = <Message>{};
  @override
  final members = <Member>{};
  @override
  late final List<String> requestedMiddlewares;
  @override
  late final Map<String, Middleware> availableMiddlewares;

  // Source specific variables
  late final String host;
  late final int port;
  late final bool ssl;
  late final String nickname;
  late final String? username;
  late final String? realname;
  late final String? password;
  late final List<String> channels;
  late final IrcClient client;

  // Constructor
  IrcSource(Map config, {Map<String, Middleware>? middlewares}) {
    // Set variables
    host = config["host"];
    port = config["port"];
    ssl = config["ssl"];
    nickname = config["nickname"];
    username = config["username"];
    realname = config["realname"];
    password = config["password"];
    if (config["channels"].runtimeType == String) {
      channels = [config["channels"]];
    } else {
      channels = List<String>.from(config["channels"]);
    }
    id = channels.join("");
    requestedMiddlewares = List<String>.from(config["middlewares"] ?? []);
    availableMiddlewares = middlewares ?? {};

    // Initialize the IRC client
    client = IrcClient(
      host: host,
      port: port,
      ssl: ssl,
      nickname: nickname,
      username: username,
      realname: realname,
      password: password,
      channels: channels,
      verbose: false,
    );
  }

  @override
  int get hashCode => Object.hash(id, type);

  @override
  bool operator ==(Object other) {
    return other is IrcSource && id == other.id && type == other.type;
  }

  @override
  Future<void> init() async {
    // Connect the IRC client
    client.connect();
  }

  @override
  Future<void> fetchHistoricalMessages() async {}

  @override
  Future<void> fetchNewMessages() async {
    if (!client.hasStoredMessages()) {
      return;
    }

    for (var m in client.getStoredMessages()) {
      // Get the message and username
      var message = m["message"] ?? "";
      var username = m["sender"] ?? "anonymous";

      // Prepare new variable
      var newMessage = Message(message.hashCode.toString(), message,
          username.hashCode.toString(), DateTime.now(), type, id, color);
      var newMember =
          Member(username.hashCode.toString(), username, type, id, color);

      // Apply middlewares
      var middlewareResult = await MiddlewareUtility.apply(
          newMessage, newMember, requestedMiddlewares, availableMiddlewares);
      newMessage = middlewareResult["message"];
      newMember = middlewareResult["member"];

      // Add message to messages
      messages.add(newMessage);

      // Add member to members
      members.add(newMember);
    }
  }

  @override
  Future<void> updateMembers() async {}

  @override
  Map<String, dynamic> transformAvatarId(Member member) {
    // TODO: implement getAvatarUrlForMember
    throw UnimplementedError();
  }
}
