import 'dart:io';
import 'constants.dart' as constants;

abstract class TemplateUtility {
  // Load template from file
  static Future<String> fromFile(File file) async {
    return await file.readAsString();
  }

  // Load template by name
  static Future<String> byName(String name, {Map? config}) async {
    var file = File('${getTemplatesPath(config)}$name');
    return await file.readAsString();
  }

  // Get template directory
  static String getTemplatesPath(Map? config) {
    final String templatesPath;

    if (config != null && config["general"]["data_path"] != null) {
      templatesPath = "${config["general"]["data_path"]}/templates/";
    } else {
      templatesPath = "${constants.defaultDataPath}templates/";
    }

    return templatesPath;
  }
}
