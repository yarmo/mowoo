import 'dart:io';
import 'package:mustache_template/mustache.dart';
import '../message.dart';
import '../member.dart';
import '../sink.dart';
import '../messages_utility.dart';
import '../template_utility.dart';

abstract class HtmlSink implements Sink {
  /// Identifier of the sink
  @override
  final String type = "html";

  static Future<String> render(List<Message> messages, Set<Member> members,
      {String? templateName, String? templateFilePath, Map? config}) async {
    // Get the plain list of messages
    List<Map<String, dynamic>> templateMessages =
        MessagesUtility.toPlainList(messages, members);

    // Prepare for HTML
    for (var i = 0; i < templateMessages.length; i++) {
      String text = templateMessages[i]["text"];
      text = text.replaceAll("\n", "<br>");
      templateMessages[i]["text"] = text;
    }

    final String templateString;
    if (templateName != null) {
      templateString =
          await TemplateUtility.byName(templateName, config: config);
    } else if (templateFilePath != null) {
      final templateFile = File(templateFilePath);
      templateString = await TemplateUtility.fromFile(templateFile);
    } else {
      throw Exception("No template was configured");
    }

    var template = Template(templateString, htmlEscapeValues: false);
    var output = template.renderString({"messages": templateMessages});

    return output;
  }
}
