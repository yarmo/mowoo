import 'dart:convert';
import '../message.dart';
import '../member.dart';
import '../sink.dart';
import '../messages_utility.dart';

abstract class JsonSink implements Sink {
  /// Identifier of the sink
  @override
  final String type = "json";

  static String render(List<Message> messages, Set<Member> members,
      {int? indentSpaces}) {
    // Init variables
    String? indent;

    // Get the plain list of messages
    List<Map<String, dynamic>> exportMessages =
        MessagesUtility.toPlainList(messages, members);

    // Generate the indentation string
    if (indentSpaces != null) {
      indent = List.filled(indentSpaces, " ").join("");
    }

    // Create the JSON encoder
    final jsonEncoder = JsonEncoder.withIndent(indent);

    return jsonEncoder.convert({"messages": exportMessages});
  }
}
