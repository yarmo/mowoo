import 'package:color/color.dart';

class Message {
  /// The unique identifier of the message
  final String id;

  /// The message's textual content
  String text;

  /// The unique identifier of the [Member] that wrote the messsage
  final String memberId;

  /// The timestamp of the message's writing
  final DateTime timestamp;

  /// The type of the [Source]
  final String sourceType;

  /// The unique identifier of the [Source]
  final String sourceId;

  /// The color of the [Source]
  final Color sourceColor;

  // Constructor
  Message(this.id, this.text, this.memberId, this.timestamp, this.sourceType,
      this.sourceId, this.sourceColor);

  @override
  int get hashCode => Object.hash(id, sourceType, sourceId);

  @override
  bool operator ==(Object other) {
    return other is Message &&
        id == other.id &&
        sourceType == other.sourceType &&
        sourceId == other.sourceId;
  }
}
